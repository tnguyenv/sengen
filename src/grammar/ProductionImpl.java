package grammar;

import java.util.LinkedList;
import java.util.Random;

public class ProductionImpl implements Production{
	private NonTerminal left;
	private RightHandSide right;
	private LinkedList<SemanticRule> semanticrules = new LinkedList<SemanticRule>();

	public ProductionImpl(){
		
	}
	
	public ProductionImpl(NonTerminal left, RightHandSide right){
		this.left = left;
		this.right = right;
	}
	
	public void setLeft(NonTerminal left) {
		this.left = left;
	}

	public void setRight(RightHandSide right) {
		this.right = right;
	}

	public NonTerminal left() {
		return left;
	}

	public RightHandSide right() {
		return right;
	}
	
	public LinkedList<SemanticRule> semanticrules(){
		return semanticrules;
	}

	public String toString() {
		return left.label() + " --> " + right.label();
	}
	
	public void addSemanticRule(SemanticRule rule){
		semanticrules.addLast(rule);
	}
	
	public void setSemanticRules(LinkedList<SemanticRule> rules){
		this.semanticrules = rules;
	}
	
	public String generate(ContextFreeGrammar CFG, /*LinkedList<SemanticRule> s*/ String number, Random r){
		System.out.println("Add rules onto the root node.");
		System.out.println(left.toString() + "(" + right.toString() + ")");
		return right.generate(CFG, number, r);
	}
}
