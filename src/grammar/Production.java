package grammar;

import java.util.LinkedList;

public interface Production extends Generable {
	public void setLeft(NonTerminal left);
	public void setRight(RightHandSide right);
	public NonTerminal left();
	public RightHandSide right();
	public LinkedList<SemanticRule> semanticrules();
	public void addSemanticRule(SemanticRule rule);
	public void setSemanticRules(LinkedList<SemanticRule> rules);
}
