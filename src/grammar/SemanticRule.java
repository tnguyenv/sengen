package grammar;

public class SemanticRule {
	private String attr;
	private String left;
	private String right;
	private String operand;

	public SemanticRule(String attr, String left, String operand, String right) {
		this.attr = attr;
		this.left = left;
		this.operand = operand;
		this.right = right;
	}

	public String attr() {
		return attr;
	}

	public String left() {
		return left;
	}

	public String right() {
		return right;
	}

	public String operand() {
		return operand;
	}

	public void setAttr(String attr) {
		this.attr = attr;
	}

	public void setLeft(String left) {
		this.left = left;
	}

	public void setRight(String right) {
		this.right = right;
	}

	public void setOperand(String operand) {
		this.operand = operand;
	}

	public String toString() {
		return left + " " + operand + " " + right;
	}

	public boolean evaluate() {
		char operation = ' ';
		if (operand.compareTo("=") == 0)
			operation = '=';
		else if (operand.compareTo("!=") == 0)
			operation = '#';
		else
			operation = '*';

		switch (operation) {
		case '=':
			if (right.toUpperCase().compareTo(left.toUpperCase()) == 0)
				return true;
			else
				return false;
		case '#':
			return true;
		}
		return false;
	}
}
