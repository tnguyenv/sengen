package grammar;

public interface NonTerminal extends Symbol {
	public String label();
	public void setLabel(String label);
	public void setStartSymbol();
	public boolean isStartSymbol();
	public String toString();
}
