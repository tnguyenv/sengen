package grammar;


public class ElementInCorpus {
	private String value;
	private String attribute;
	
	public ElementInCorpus(String value, String attribute){
		this.value = value;
		this.attribute = attribute;
	}
	
	public String value(){
		return value;

	}
	
	public String attribute() {
		return attribute;
	}
	
	public String toString(){
		return "(" + value + ", " + attribute + ")";
	}
}
