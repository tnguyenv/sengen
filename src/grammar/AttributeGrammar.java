/**
AG: Attribute Grammar
 */
package grammar;

import java.util.Set;

public class AttributeGrammar {
	private ContextFreeGrammar  CFG;
	private Set<Attribute>  AttX;
	private Set<Production> SemRules;
	
	public AttributeGrammar(ContextFreeGrammar CFG, Set<Attribute> AttX, Set<Production> SemRules)
	{
		this.CFG = CFG;
		this.AttX = AttX;
		this.SemRules = SemRules;
	}
	
	public ContextFreeGrammar CFG(){
		return CFG;
	}
	
	public Set<Attribute> AttX(){
		return AttX;
	}
	
	public Set<Production> SemRules(){
		return SemRules;
	}
}
