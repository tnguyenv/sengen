package grammar;

import java.util.LinkedList;

public interface RightHandSide extends Generable {
	public String label();
	public boolean setSatisfy(LinkedList<SemanticRule> Rules);
}
