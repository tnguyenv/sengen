package grammar;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;
import java.util.StringTokenizer;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class ContextFreeGrammar {
	private LinkedList<Terminal> Terminals = new LinkedList<Terminal>();
	private LinkedList<NonTerminal> NonTerminals = new LinkedList<NonTerminal>();
	private NonTerminal Start = null;
	private LinkedList<Production> Productions = new LinkedList<Production>();
	public ContextFreeGrammar() {

	}

	public ContextFreeGrammar(LinkedList<Terminal> terminals,
			LinkedList<NonTerminal> nonterminals,
			LinkedList<Production> productions) {
		this.Terminals = terminals;
		this.NonTerminals = nonterminals;
		this.Productions = productions;
	}

	public LinkedList<Terminal> Terminals() {
		return Terminals;
	}

	public LinkedList<NonTerminal> NonTerminals() {
		return NonTerminals;
	}

	public LinkedList<Production> Productions() {
		return Productions;
	}

	public void addTerminal(Terminal terminal) {
		Terminals.addLast(terminal);
	}

	public void addNonTerminal(NonTerminal nonterminal) {
		NonTerminals.addLast(nonterminal);
	}

	public void addProduction(Production rule) {
		Productions.addLast(rule);
	}

	public NonTerminal Start() {
		return Start;
	}

	public void setStartSymbol(NonTerminal start) {
		this.Start = start;
	}

	public String generate() {
		/*
		 * Step 1: Look up the Rule corresponding to the start variable and call
		 * generate( ) on it, passing itself (this) as a parameter. Step 2:
		 */
		Production production = randomChoose();
		// To Test
		// ////////////////////////////////
		return production.generate(this, /* production.semanticrules() */
				"number", new Random());
	}

	protected Production randomChoose() {
		Production p = new ProductionImpl();
		Random random = new Random();
		p = Productions.get(random.nextInt(Productions.size()));
		return p;
	}

	public boolean isNonTerminal(String s) {
		s = s.toUpperCase();
		for (NonTerminal nonter : NonTerminals)
			if (nonter.label().toUpperCase().compareTo(s) == 0)
				return true;
		return false;
	}

	public boolean isTerminal(String s) {
		s = s.toUpperCase();
		for (Terminal ter : Terminals)
			if (ter.label().toUpperCase().compareTo(s) == 0)
				return true;
		return false;
	}

	public void loadGrammar(int opt) {
		File file;
		File fileTerminal = new File("test/Terminal.txt");
		File fileNonTerminal = new File("test/NonTerminal.txt");
		String folder = "test/";
		Scanner scanner = null;
		String filename = "", aLine = "";

		try {
			if (opt == 0) // Automatically load grammar on the default folder
			{
				fileTerminal = new File("test/Terminal.txt");
				fileNonTerminal = new File("test/NonTerminal.txt");
				folder =  "test/";
			} else // Display dialog box to user choose the folder which
					// contains files of the grammar
			{
				JFileChooser fileChooser;
				int status;
				fileChooser = new JFileChooser("."); // current folder
				fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				fileChooser.setDialogTitle("Choose the files contain Terminals and Nonterminals");
				fileChooser.setMultiSelectionEnabled(true);
				status = fileChooser.showOpenDialog(null);
				if (status == JFileChooser.APPROVE_OPTION) {
					File[] lstFile;
					lstFile = fileChooser.getSelectedFiles();
					for (File elt : lstFile)
						if (elt.getName().equals("Terminal.txt"))
							fileTerminal = elt;
						else if (elt.getName().equals("NonTerminal.txt"))
							fileNonTerminal = elt;
					folder = fileChooser.getCurrentDirectory().getName() + "/";
					// Check fileTerminal and fileNonTerminal		
				} else {
					JOptionPane
							.showMessageDialog(null,
									"You haven't choosen any file. The program will load the default file.");
				}
			}

			// Terminal
			scanner = new Scanner(fileTerminal);
			while (scanner.hasNextLine()) {
				aLine = scanner.nextLine();
				Terminal terminal = new TerminalImpl(aLine);
				this.addTerminal(terminal);
			}

			// NonTerminal
			scanner = new Scanner(fileNonTerminal);
			// Read and set the start symbol
			aLine = scanner.nextLine();
			NonTerminal start = new NonTerminalImpl(aLine, true);
			this.setStartSymbol(start);
			this.addNonTerminal(start);

			while (scanner.hasNextLine()) {
				aLine = scanner.nextLine();
				NonTerminal nonterminal = new NonTerminalImpl(aLine, false);
				this.addNonTerminal(nonterminal);
			}

			// Productions
			for (NonTerminal nonterminal : this.NonTerminals()) {
				filename = folder + nonterminal.label() + ".txt";
				file = new File(filename);
				scanner = new Scanner(file);

				while (scanner.hasNextLine()) {
					aLine = scanner.nextLine();
					// Treat productions
					LinkedList<String> token = new LinkedList<String>();
					String semantic = "";
					if (aLine.indexOf("{") > 0) {
						token = tokenize(aLine.substring(0, aLine.indexOf("{"))
								.trim());
						semantic = aLine.substring(aLine.lastIndexOf("{") + 1,
								aLine.lastIndexOf("}")).trim();
					} else
						token = tokenize(aLine.trim());
					LinkedList<Symbol> symbol = new LinkedList<Symbol>();
					Symbol s;
					for (String e : token) {
						if (this.isTerminal(e))
							s = new TerminalImpl(e);
						else
							s = new NonTerminalImpl(e, false);
						symbol.add(s);
					}

					RightHandSide rhs = new RightHandSideImpl(symbol);
					Production aProduction = new ProductionImpl();
					aProduction.setLeft(nonterminal);
					aProduction.setRight(rhs);

					// Treat semantic rules
					if (!semantic.isEmpty()) {
						Iterator<String> it = tokenize(semantic).iterator();
						while (it.hasNext()) {
							String attr = "number";
							SemanticRule rule = new SemanticRule(attr, it
									.next(), it.next(), it.next());
							aProduction.addSemanticRule(rule);
						}
					}
					// Attach the production to grammar
					this.addProduction(aProduction);
				}
			}
			// Corpus
			String value = "";
			String attribute = "";
			for (Terminal ter : this.Terminals()) {
				filename = folder + ter.label() + ".txt";
				file = new File(filename);
				scanner = new Scanner(file);

				while (scanner.hasNextLine()) {
					aLine = scanner.nextLine();
					value = aLine.substring(0, aLine.indexOf("|"));
					attribute = aLine.substring(aLine.lastIndexOf("|") + 1);
					ter.addCorpus(value.trim(), attribute.trim());
				}
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			System.out.println("File isn't found.");
			System.exit(0);
		}
	} // loadGrammar
	
	private LinkedList<String> tokenize(String input) {
		LinkedList<String> list = new LinkedList<String>();
		StringTokenizer t = new StringTokenizer(input);

		while (t.hasMoreElements())
			list.addLast(t.nextToken());
		return list;
	}
}