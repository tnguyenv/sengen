package grammar;

import java.util.Random;

public interface Generable{
	public String generate(ContextFreeGrammar g, /*LinkedList<SemanticRule> rules*/ String number, Random r);
}
