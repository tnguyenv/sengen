package grammar;

import java.util.LinkedList;
import java.util.Random;

public class NonTerminalImpl implements NonTerminal {
	private String label;
	private boolean start = false;
	private String attribute;
	
	public NonTerminalImpl(String label){
		this.label = label;
	}
	
	public NonTerminalImpl(String label, boolean start){
		this.label = label;
		this.start = start;
	}
	
	public void setStartSymbol()
	{
		start = true;
	}
	
	public boolean isStartSymbol()
	{
		return start;
	}
	
	public String label()
	{
		return label;
	}
	
	public String attribute(){
		return attribute;
	}
	
	public void setLabel(String v)
	{
		this.label = v;
	}
	
	public void setAttribute(String attribute){
		this.attribute = attribute;
	}
	
	public String generate(ContextFreeGrammar g, /*LinkedList<SemanticRule> semanticrules */String number, Random r){
		/*
		 * Random choose a production from g that its left hand side is equal the label
		 * */
		LinkedList<Production> productions = new LinkedList<Production>();
		for (Production production: g.Productions())
			if (production.left().label().compareTo(label)==0)
				productions.addLast(production);
		if (!productions.isEmpty()){
			Production production = productions.get(r.nextInt(productions.size()));
			// To Test
			//System.out.println("Luat duoc chon (tu NonTerminal): " + production.toString());
			//////////////////////////////////
			return production.generate(g,number,r);
		}
		else
			return label;
	}
	
	public String toString(){
		return label;
	}
}
