package grammar;

import java.util.LinkedList;
import java.util.Random;

public class RightHandSideImpl implements RightHandSide {
	private LinkedList<Symbol> sequence = new LinkedList<Symbol>();
	private Production production = new ProductionImpl();

	public RightHandSideImpl(LinkedList<Symbol> sequence) {
		this.sequence = sequence;
	}

	public LinkedList<Symbol> sequence() {
		return sequence;
	}

	public Symbol sequence(int index) {
		return sequence.get(index);
	}

	public String generate(ContextFreeGrammar CFG,
			/*LinkedList<SemanticRule> rules*/ String number, Random r) {
		// Get the production of the right hand side
		for (Production pro : CFG.Productions())
			if (pro.right().label().compareTo(this.toString()) == 0) {
				production = pro;
			}
		System.out.println(production);
		// Make the semantic rules of production satisfied all the symbol
		//if (!production.semanticrules().isEmpty())
		//	setSatisfy(production.semanticrules());

		// Call the method generate() of each symbol of the sequence
		String str = "";
		number = (new Random().nextInt(2)==0)?"singular":"plural";
		//String s = sequence.get(0).generate(CFG, number, r);
		//String num = "";
		
		for (Symbol symbol : sequence) {
			
			str += symbol.generate(CFG, number, r) + " ";
		}
		return Sentencecase(str.trim());
	}

	public void addSymbol(Symbol s) {
		this.sequence.addLast(s);
	}

	public String toString() {
		String str = "";
		for (Symbol symbol : sequence)
			str += symbol.label() + " ";
		return str.trim();
	}

	public String label() {
		String str = "";
		for (Symbol symbol : sequence)
			str += symbol.label() + " ";
		return str.trim();
	}

	public boolean setSatisfy(LinkedList<SemanticRule> Rules) {
		/*
		 * Every rule in Rules will checked on the sequence
		 */
		for (SemanticRule rule : Rules) {
			System.out.println("Apply the rule: " + rule.toString());
			//for (Symbol symbol : sequence)
				return false;
		}
		return true;
	}
	
	public static String Sentencecase(String str){
		String s = str.substring(0,1).toUpperCase();
		s += str.substring(1);
		s += ".";
		return s;
	}
}