package grammar;

import java.util.LinkedList;
import java.util.Random;

public class TerminalImpl implements Terminal {
	private String label;
	private String number="";
	private LinkedList<ElementInCorpus> corpus = new LinkedList<ElementInCorpus>();
		
	public TerminalImpl(String label){
		this.label = label;
		//this.number = (new Random().nextInt(2)==0)?"singular":"plural";
	}
	
	public String label(){	
		return label;
	}
	
	public void setLabel(String label){
		this.label = label; 
	}
	
	public LinkedList<ElementInCorpus> corpus(){
		return corpus;
	}
	
	public void addCorpus(ElementInCorpus element){
		corpus.addLast(element);
	}
	
	public void addCorpus(String value, String attribute){
		corpus.addLast(new ElementInCorpus(value,attribute));
	}
	
	public String generate(ContextFreeGrammar g, /*LinkedList<SemanticRule> rules*/ String number, Random r){
		for(Terminal ter: g.Terminals())
			if (ter.label().compareTo(label)==0){
				// Create two lists contain singular and plural words
				LinkedList<ElementInCorpus> sing = new LinkedList<ElementInCorpus>();
				LinkedList<ElementInCorpus> plur = new LinkedList<ElementInCorpus>();
				for (ElementInCorpus e: ter.corpus())
					if (e.attribute().compareTo("singular")==0) 
						sing.addLast(e);
					else
						plur.addLast(e);
				
				ElementInCorpus element;
				
				//if (ter.number().compareTo("singular")==0)// && number.compareTo("singular")==0)
				if (number.compareTo("singular")==0)
					element = sing.get(r.nextInt(sing.size()));
				else
					element = plur.get(r.nextInt(plur.size()));
				
				setNumber(element.attribute());
				return element.value();
			}
		return "";
	}
	
	public Terminal getTerminal(){
		return this;
	}
	// Define attributes
	public String number(){
		return number;
	}
	
	public void setNumber(String number){
		this.number = number;
	}
}