package grammar;

import java.util.LinkedList;

public interface Terminal extends Symbol {
	public String label();
	public void setLabel(String label);
	public LinkedList<ElementInCorpus> corpus();
	public void addCorpus(ElementInCorpus element);
	public void addCorpus(String value, String attribute);
	
	public String number();
	public void setNumber(String number);
	public Terminal getTerminal();
}