import gui.MainFrame;

public class MainPro {
	private static final long serialVersionUID = 1L;
	public static void main(String[] args){
		final MainFrame frame = new MainFrame();

		frame.setExtendedState(javax.swing.JFrame.MAXIMIZED_BOTH);
		frame.setResizable(false);
	    
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				frame.setVisible(true);
			}
		});
	}
}