package gui;

import grammar.ContextFreeGrammar;
import grammar.NonTerminal;
import grammar.Production;
import grammar.Terminal;

import javax.swing.JFrame;

public class TextGeneration {
	private static ContextFreeGrammar CFG;

	public TextGeneration(){
		CFG = new ContextFreeGrammar();
		CFG.loadGrammar(0);
		final MainFrame frame = new MainFrame();
		
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				frame.setVisible(true);
			}
		});
		
		String str = "";
		for (Terminal ter : CFG.Terminals()) {
			str += ter.label() + "\n";
		}
		str = str.substring(0, str.length() - 1);
		frame.addTer(str);
		frame.addStart(CFG.Start().label());

		str = "";
		for (NonTerminal nonter : CFG.NonTerminals()) {
			str += nonter.label() + "\n";
		}
		str = str.substring(0, str.length() - 1);
		frame.addNonTer(str);

		str = "";
		for (Production production : CFG.Productions()) {
			str += production.toString() + "\n";
		}
		str = str.substring(0, str.length() - 1);
		frame.addRule(str);
		str = CFG.generate();
		frame.addSentence(str);
	}
	
	public void start(){
		javax.swing.SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				JFrame.setDefaultLookAndFeelDecorated(true);
				MainFrame mainFrame = new MainFrame();
				mainFrame.setLocationRelativeTo(null);
				mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			}
		});
	}
}
