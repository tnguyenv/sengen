package gui;

import grammar.ContextFreeGrammar;
import grammar.NonTerminal;
import grammar.Production;
import grammar.Terminal;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.GroupLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.text.DefaultEditorKit;

import utilities.TextUtilities;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class MainFrame extends javax.swing.JFrame implements ActionListener {

    private static final long serialVersionUID = 1L;
    private ContextFreeGrammar CFG = new ContextFreeGrammar();

    // Variables declaration - do not modify
    private javax.swing.JButton btnGenerate;
    private javax.swing.JButton btnModify;
    private javax.swing.JLabel lblRules;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu mnuFile;
    private javax.swing.JMenu mnuEdit;
    private javax.swing.JMenuBar mnuMainMenu;
    private javax.swing.JMenuItem mnuFileLoad;
    private javax.swing.JScrollPane scrollPanelRules;
    private javax.swing.JScrollPane scrollPanelTerminals;
    private javax.swing.JScrollPane scrollPanelStartSymbol;
    private javax.swing.JScrollPane scrollPanelNonTerminals;
    private javax.swing.JScrollPane scrollPanelCorpus;
    private javax.swing.JScrollPane scrollPanelResults;
    private javax.swing.JTextArea txtAreaRules;
    private javax.swing.JTextArea txtAreaTerminals;
    private javax.swing.JTextArea txtAreaStartSymbol;
    private javax.swing.JTextArea txtAreaNonTerminals;
    private javax.swing.JTextArea txtAreaCorpus;
    private JTextField txtNumberSentences;
    private JLabel jLabel7;
    private JTextArea txtAreaStatus;
    private javax.swing.JTextArea txtAreaResults;
    private JMenuItem mnuHelpAbout;
    private JSeparator jSeparator6;
    private JMenuItem mnuHelpContents;
    private JSeparator jSeparator5;
    private JMenuItem mnuHelpWelcome;
    private JMenu mnuHelp;
    private JMenuItem mnuEditSelectAll;
    private JMenuItem mnuEditDelete;
    private JSeparator jSeparator4;
    private JMenuItem mnuEditPaste;
    private JMenuItem mnuEditCopy;
    private JMenuItem mnuEditCut;
    private JSeparator jSeparator3;
    private JMenuItem mnuEditRedo;
    private JMenuItem mnuEditUndo;
    private JSeparator jSeparator2;
    private JMenuItem mnuFileSaveAs;
    private JMenuItem mnuFileSave;
    private JMenuItem mnuFileQuit;
    private JMenuItem mnuFileNewGrammar;
    private JSeparator jSeparator1;

    // End of variables declaration
    public MainFrame() {
        initComponents(); // Decorate a frame

        CFG.loadGrammar(0); // Load grammar into memory and add them onto the
        // frame

        String str = "";
        for (Terminal ter : CFG.Terminals()) {
            str += ter.label() + "\n";
        }
        str = str.substring(0, str.length() - 1);
        this.addTer(str);
        this.addStart(CFG.Start().label());

        str = "";
        for (NonTerminal nonter : CFG.NonTerminals()) {
            str += nonter.label() + "\n";
        }
        str = str.substring(0, str.length() - 1);
        this.addNonTer(str);

        str = "";
        for (Production production : CFG.Productions()) {
            str += production.toString() + "\n";
        }
        str = str.substring(0, str.length() - 1);
        this.addRule(str);
        str = CFG.generate();
        this.addSentence(str);

        // Set the position of the frame is center of the screen
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension screenSize = tk.getScreenSize();
        int screenHeight = screenSize.height;
        int screenWidth = screenSize.width;
        setLocation((screenWidth - this.getSize().width) / 2,
                (screenHeight - this.getSize().height) / 2);
    }

    private void initComponents() {

        scrollPanelRules = new javax.swing.JScrollPane();
        txtAreaRules = new javax.swing.JTextArea();
        lblRules = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        scrollPanelTerminals = new javax.swing.JScrollPane();
        txtAreaTerminals = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        scrollPanelStartSymbol = new javax.swing.JScrollPane();
        txtAreaStartSymbol = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        scrollPanelNonTerminals = new javax.swing.JScrollPane();
        txtAreaNonTerminals = new javax.swing.JTextArea();
        jLabel5 = new javax.swing.JLabel();
        scrollPanelCorpus = new javax.swing.JScrollPane();
        txtAreaCorpus = new javax.swing.JTextArea();
        jLabel6 = new javax.swing.JLabel();
        scrollPanelResults = new javax.swing.JScrollPane();
        txtAreaResults = new javax.swing.JTextArea();
        btnGenerate = new javax.swing.JButton();
        btnModify = new javax.swing.JButton();
        GridLayout btnModifyLayout = new GridLayout(1, 1);
        btnModifyLayout.setColumns(1);
        btnModifyLayout.setHgap(5);
        btnModifyLayout.setVgap(5);
        btnModify.setLayout(btnModifyLayout);
        mnuMainMenu = new javax.swing.JMenuBar();
        mnuFile = new javax.swing.JMenu();
        mnuFileLoad = new javax.swing.JMenuItem();
        mnuEdit = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        this.setTitle("Text generation from an attribute grammar \uu00a9 2008 Nguyen Vu Ngoc Tung - Nguyen Thien Binh");

        txtAreaRules.setColumns(20);
        txtAreaRules.setRows(5);
        scrollPanelRules.setViewportView(txtAreaRules);
        txtAreaRules.setPreferredSize(new java.awt.Dimension(252, 82));

        lblRules.setText("These are rules:");

        jLabel2.setText("These are terminals:");

        txtAreaTerminals.setColumns(20);
        txtAreaTerminals.setRows(5);
        scrollPanelTerminals.setViewportView(txtAreaTerminals);

        jLabel3.setText("This is start symbol:");

        txtAreaStartSymbol.setColumns(20);
        txtAreaStartSymbol.setRows(5);
        scrollPanelStartSymbol.setViewportView(txtAreaStartSymbol);
        txtAreaStartSymbol.setPreferredSize(new java.awt.Dimension(252, 82));

        jLabel4.setText("These are nonterminal:");

        txtAreaNonTerminals.setColumns(20);
        txtAreaNonTerminals.setRows(5);
        scrollPanelNonTerminals.setViewportView(txtAreaNonTerminals);
        txtAreaNonTerminals.setPreferredSize(new java.awt.Dimension(263, 82));

        jLabel5.setText("These are corpus:");

        txtAreaCorpus.setColumns(20);
        txtAreaCorpus.setRows(5);
        scrollPanelCorpus.setViewportView(txtAreaCorpus);

        jLabel6.setText("This is the random sentence: ");

        txtAreaResults.setColumns(20);
        txtAreaResults.setRows(5);
        txtAreaResults.setLineWrap(true);
        txtAreaResults.setEditable(true);
        txtAreaResults.setWrapStyleWord(true);
        scrollPanelResults.setViewportView(txtAreaResults);
        txtAreaResults.setPreferredSize(new java.awt.Dimension(262, 82));

        Action[] actions = txtAreaResults.getActions();
        Action cutAction = TextUtilities.findAction(actions,
                DefaultEditorKit.cutAction);
        Action copyAction = TextUtilities.findAction(actions,
                DefaultEditorKit.copyAction);
        Action pasteAction = TextUtilities.findAction(actions,
                DefaultEditorKit.pasteAction);

        btnGenerate.setText("Generate");
        btnModify.setText("Modify");
        {
            txtAreaStatus = new JTextArea();
        }
        {
            jLabel7 = new JLabel();
            jLabel7.setText("Number of sentences");
        }
        {
            txtNumberSentences = new JTextField();
        }
        btnGenerate.addActionListener(this);
        btnModify.addActionListener(this);

        mnuFile.setText("File");
        {
            mnuFileNewGrammar = new JMenuItem();
            mnuFile.add(mnuFileNewGrammar);
            mnuFileNewGrammar.setText("Define a new grammar");
            mnuFileNewGrammar.addActionListener(this);
        }

        mnuFileLoad.setText("Load grammar from...");
        mnuFileLoad.addActionListener(this);
        mnuFile.add(mnuFileLoad);
        {
            jSeparator1 = new JSeparator();
            mnuFile.add(jSeparator1);
        }
        {
            mnuFileSave = new JMenuItem();
            mnuFile.add(mnuFileSave);
            mnuFileSave.setText("Save");
            mnuFileSave.addActionListener(this);
        }
        {
            mnuFileSaveAs = new JMenuItem();
            mnuFile.add(mnuFileSaveAs);
            mnuFileSaveAs.setText("Save As...");
            mnuFileSaveAs.addActionListener(this);
        }
        {
            jSeparator2 = new JSeparator();
            mnuFile.add(jSeparator2);
        }
        {
            mnuFileQuit = new JMenuItem();
            mnuFile.add(mnuFileQuit);
            mnuFileQuit.setText("Quit");
            mnuFileQuit.addActionListener(this);
        }

        mnuMainMenu.add(mnuFile);

        mnuEdit.setText("Edit");
        mnuMainMenu.add(mnuEdit);
        {
            mnuHelp = new JMenu();
            mnuMainMenu.add(mnuHelp);
            mnuHelp.setText("Help");
            {
                mnuHelpWelcome = new JMenuItem();
                mnuHelp.add(mnuHelpWelcome);
                mnuHelpWelcome.setText("Welcome");
                mnuHelpWelcome.addActionListener(this);
            }
            {
                jSeparator5 = new JSeparator();
                mnuHelp.add(jSeparator5);
            }
            {
                mnuHelpContents = new JMenuItem();
                mnuHelp.add(mnuHelpContents);
                mnuHelpContents.setText("Contents");
                mnuHelpContents.addActionListener(this);
            }
            {
                jSeparator6 = new JSeparator();
                mnuHelp.add(jSeparator6);
            }
            {
                mnuHelpAbout = new JMenuItem();
                mnuHelp.add(mnuHelpAbout);
                mnuHelpAbout.setText("About");
                mnuHelpAbout.addActionListener(this);
            }
        }
        {
            mnuEditUndo = new JMenuItem();
            mnuEdit.add(mnuEditUndo);
            mnuEditUndo.setText("Undo");
            mnuEditUndo.addActionListener(this);
        }
        {
            mnuEditRedo = new JMenuItem();
            mnuEdit.add(mnuEditRedo);
            mnuEditRedo.setText("Redo");
            mnuEditRedo.addActionListener(this);
        }
        {
            jSeparator3 = new JSeparator();
            mnuEdit.add(jSeparator3);
        }
        {
            mnuEditCut = new JMenuItem(cutAction);
            mnuEdit.add(mnuEditCut);
            mnuEditCut.setText("Cut");
            mnuEditCut.addActionListener(this);
        }
        {
            mnuEditCopy = new JMenuItem(copyAction);
            mnuEdit.add(mnuEditCopy);
            mnuEditCopy.setText("Copy");
            mnuEditCopy.addActionListener(this);
        }
        {
            mnuEditPaste = new JMenuItem(pasteAction);
            mnuEdit.add(mnuEditPaste);
            mnuEditPaste.setText("Paste");
            mnuEditPaste.addActionListener(this);
        }
        {
            jSeparator4 = new JSeparator();
            mnuEdit.add(jSeparator4);
        }
        {
            mnuEditDelete = new JMenuItem();
            mnuEdit.add(mnuEditDelete);
            mnuEditDelete.setText("Delete");
            mnuEditDelete.addActionListener(this);
        }
        {
            mnuEditSelectAll = new JMenuItem();
            mnuEdit.add(mnuEditSelectAll);
            mnuEditSelectAll.setText("Select All");
            mnuEditSelectAll.addActionListener(this);
        }

        setJMenuBar(mnuMainMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
                getContentPane());
        getContentPane().setLayout(layout);
        layout.setVerticalGroup(layout.createSequentialGroup().addGroup(
                layout.createParallelGroup(
                GroupLayout.Alignment.BASELINE).addComponent(jLabel4,
                GroupLayout.Alignment.BASELINE,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE).addComponent(jLabel2,
                GroupLayout.Alignment.BASELINE,
                GroupLayout.PREFERRED_SIZE, 14,
                GroupLayout.PREFERRED_SIZE).addComponent(lblRules,
                GroupLayout.Alignment.BASELINE,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE)).addPreferredGap(
                LayoutStyle.ComponentPlacement.UNRELATED).addGroup(
                layout.createParallelGroup().addComponent(
                scrollPanelNonTerminals,
                GroupLayout.Alignment.LEADING,
                GroupLayout.PREFERRED_SIZE, 85,
                GroupLayout.PREFERRED_SIZE).addComponent(scrollPanelTerminals,
                GroupLayout.Alignment.LEADING,
                GroupLayout.PREFERRED_SIZE, 85,
                GroupLayout.PREFERRED_SIZE).addComponent(scrollPanelRules,
                GroupLayout.Alignment.LEADING,
                GroupLayout.PREFERRED_SIZE, 85,
                GroupLayout.PREFERRED_SIZE)).addGap(18).addGroup(
                layout.createParallelGroup(
                GroupLayout.Alignment.BASELINE).addComponent(jLabel6,
                GroupLayout.Alignment.BASELINE,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE).addComponent(jLabel5,
                GroupLayout.Alignment.BASELINE,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE).addComponent(jLabel3,
                GroupLayout.Alignment.BASELINE,
                GroupLayout.PREFERRED_SIZE, 14,
                GroupLayout.PREFERRED_SIZE)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(
                layout.createParallelGroup().addComponent(
                scrollPanelResults,
                GroupLayout.Alignment.LEADING,
                GroupLayout.PREFERRED_SIZE, 85,
                GroupLayout.PREFERRED_SIZE).addComponent(scrollPanelCorpus,
                GroupLayout.Alignment.LEADING,
                GroupLayout.PREFERRED_SIZE, 85,
                GroupLayout.PREFERRED_SIZE).addComponent(scrollPanelStartSymbol,
                GroupLayout.Alignment.LEADING,
                GroupLayout.PREFERRED_SIZE, 85,
                GroupLayout.PREFERRED_SIZE)).addPreferredGap(
                LayoutStyle.ComponentPlacement.UNRELATED).addGroup(
                layout.createParallelGroup().addComponent(txtAreaStatus,
                GroupLayout.Alignment.LEADING,
                0, 129, Short.MAX_VALUE).addGroup(
                GroupLayout.Alignment.LEADING,
                layout.createSequentialGroup().addComponent(
                jLabel7,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE).addPreferredGap(
                LayoutStyle.ComponentPlacement.RELATED).addGroup(
                layout.createParallelGroup(
                GroupLayout.Alignment.BASELINE).addComponent(
                txtNumberSentences,
                GroupLayout.Alignment.BASELINE,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE).addComponent(
                btnGenerate,
                GroupLayout.Alignment.BASELINE,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE))).addGap(18)).addGroup(
                layout.createParallelGroup(
                GroupLayout.Alignment.BASELINE).addComponent(btnModify,
                GroupLayout.Alignment.CENTER,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE)).addContainerGap());

        layout.setHorizontalGroup(layout.createSequentialGroup().addContainerGap(20, 20).addGroup(
                layout.createParallelGroup().addGroup(
                layout.createSequentialGroup().addGroup(
                layout.createParallelGroup().addComponent(
                scrollPanelStartSymbol,
                GroupLayout.Alignment.LEADING,
                GroupLayout.PREFERRED_SIZE,
                256,
                GroupLayout.PREFERRED_SIZE).addGroup(
                GroupLayout.Alignment.LEADING,
                layout.createSequentialGroup().addComponent(
                jLabel3,
                GroupLayout.PREFERRED_SIZE,
                135,
                GroupLayout.PREFERRED_SIZE).addGap(
                121)).addComponent(
                scrollPanelRules,
                GroupLayout.Alignment.LEADING,
                GroupLayout.PREFERRED_SIZE,
                256,
                GroupLayout.PREFERRED_SIZE).addGroup(
                GroupLayout.Alignment.LEADING,
                layout.createSequentialGroup().addComponent(
                lblRules,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE).addGap(
                175))).addGap(17).addGroup(
                layout.createParallelGroup().addComponent(
                scrollPanelCorpus,
                GroupLayout.Alignment.LEADING,
                GroupLayout.PREFERRED_SIZE,
                271,
                GroupLayout.PREFERRED_SIZE).addGroup(
                GroupLayout.Alignment.LEADING,
                layout.createSequentialGroup().addComponent(
                jLabel5,
                GroupLayout.PREFERRED_SIZE,
                123,
                GroupLayout.PREFERRED_SIZE).addGap(
                148)).addComponent(
                scrollPanelTerminals,
                GroupLayout.Alignment.LEADING,
                GroupLayout.PREFERRED_SIZE,
                271,
                GroupLayout.PREFERRED_SIZE).addGroup(
                GroupLayout.Alignment.LEADING,
                layout.createSequentialGroup().addComponent(
                jLabel2,
                GroupLayout.PREFERRED_SIZE,
                111,
                GroupLayout.PREFERRED_SIZE).addGap(
                160)))).addComponent(txtAreaStatus,
                GroupLayout.Alignment.LEADING,
                GroupLayout.PREFERRED_SIZE,
                544, GroupLayout.PREFERRED_SIZE)).addGap(34).addGroup(
                layout.createParallelGroup().addComponent(scrollPanelResults,
                GroupLayout.Alignment.LEADING,
                GroupLayout.PREFERRED_SIZE,
                271, GroupLayout.PREFERRED_SIZE).addGroup(
                GroupLayout.Alignment.LEADING,
                layout.createSequentialGroup().addComponent(
                jLabel6,
                GroupLayout.PREFERRED_SIZE,
                175,
                GroupLayout.PREFERRED_SIZE).addGap(96)).addComponent(scrollPanelNonTerminals,
                GroupLayout.Alignment.LEADING,
                GroupLayout.PREFERRED_SIZE,
                271, GroupLayout.PREFERRED_SIZE).addGroup(
                GroupLayout.Alignment.LEADING,
                layout.createSequentialGroup().addGroup(
                layout.createParallelGroup().addComponent(
                jLabel4,
                GroupLayout.Alignment.LEADING,
                GroupLayout.PREFERRED_SIZE,
                132,
                GroupLayout.PREFERRED_SIZE).addComponent(
                jLabel7,
                GroupLayout.Alignment.LEADING,
                GroupLayout.PREFERRED_SIZE,
                130,
                GroupLayout.PREFERRED_SIZE).addGroup(
                GroupLayout.Alignment.LEADING,
                layout.createSequentialGroup().addComponent(
                txtNumberSentences,
                GroupLayout.PREFERRED_SIZE,
                91,
                GroupLayout.PREFERRED_SIZE).addGap(
                41))).addGap(30).addComponent(
                btnGenerate,
                GroupLayout.PREFERRED_SIZE,
                91,
                GroupLayout.PREFERRED_SIZE)).addComponent(btnModify,
                GroupLayout.PREFERRED_SIZE, 100,
                GroupLayout.PREFERRED_SIZE)).addGap(6));

        pack();
        this.setSize(891, 481);
    }

    public void addRule(String str) {
        this.txtAreaRules.setText(str);
    }

    public void addTer(String str) {
        this.txtAreaTerminals.setText(str);
    }

    public void addStart(String str) {
        this.txtAreaStartSymbol.setText(str);
    }

    public void addNonTer(String str) {
        this.txtAreaNonTerminals.setText(str);
    }

    public void addCorpus(String str) {
        this.txtAreaCorpus.setText(str);
    }

    public void addSentence(String str) {
        this.txtAreaResults.setText(str);
    }

    public void addStatus(String str) {
        this.txtAreaStatus.append(str);
    }

    public void actionPerformed(ActionEvent e) {
        String str = e.getActionCommand();
        if (str.equals("Quit")) {
            int returnval = JOptionPane.NO_OPTION;
            returnval = JOptionPane.showConfirmDialog(this,
                    "Do you really want to quit the program?", "Exit System",
                    JOptionPane.YES_NO_OPTION);
            if (returnval == JOptionPane.YES_OPTION) {
                System.exit(0);
            }
        } else if (str.equals("Define a new grammar")) {
            // Clear text at all the controls
            txtAreaTerminals.setText(null);
            txtAreaRules.setText(null);
            txtAreaNonTerminals.setText(null);
            txtAreaStartSymbol.setText(null);
            txtAreaCorpus.setText(null);
            txtAreaResults.setText(null);
            // Enable all controls
            txtAreaTerminals.setEnabled(true);
            txtAreaRules.setEnabled(true);
            txtAreaNonTerminals.setEnabled(true);
            txtAreaStartSymbol.setEnabled(true);
            txtAreaCorpus.setEnabled(true);
            txtAreaResults.setEnabled(true);
        } else if (str.equals("Generate")) {
            str = CFG.generate();
            this.addSentence(str);
        } else if (str.equals("Welcome")) {
            JOptionPane.showConfirmDialog(this, "U have pressed Welcome.",
                    "message", JOptionPane.CLOSED_OPTION);
        } else if (str.equals("Contents")) {
            JOptionPane.showConfirmDialog(this, "U have pressed Contents.",
                    "message", JOptionPane.CLOSED_OPTION);
        } else if (str.equals("About")) {
            MyAbout ctb = new MyAbout("About Text Generation");
            ctb.setSize(300, 200);
            ctb.setVisible(true);
            ctb.setResizable(false);
        } else if (str.equals("Load grammar from...")) {
            CFG.loadGrammar(1);
        } else if (str.equals("Save")) {
            JOptionPane.showConfirmDialog(this, "U have pressed Save.",
                    "message", JOptionPane.CLOSED_OPTION);
        } else if (str.equals("Save As...")) {
            JOptionPane.showConfirmDialog(this, "U have pressed Save As.",
                    "message", JOptionPane.CLOSED_OPTION);
        } else if (str.equals("Undo")) {
            JOptionPane.showConfirmDialog(this, "U have pressed Undo.",
                    "message", JOptionPane.CLOSED_OPTION);
        } else if (str.equals("Redo")) {
            JOptionPane.showConfirmDialog(this, "U have pressed Redo.",
                    "message", JOptionPane.CLOSED_OPTION);

        } else if (str.equals("Cut")) {
        } else if (str.equals("Copy")) {
        } else if (str.equals("Paste")) {
        } else if (str.equals("Delete")) {
            JOptionPane.showConfirmDialog(this, "U have pressed Delete.",
                    "message", JOptionPane.CLOSED_OPTION);
        } else if (str.equals("Select All")) {
            JOptionPane.showConfirmDialog(this, "U have pressed Select All.",
                    "message", JOptionPane.CLOSED_OPTION);
        }
    }

    class MyAbout extends JDialog {

        private static final long serialVersionUID = 1L;
        private JLabel infor;
        private JLabel jLabel2;
        private JLabel lblRules;

        public MyAbout(String title) {
            super();
            this.setTitle(title);
            initGUI();
        }

        @Override
        public void setSize(int width, int height) {
            super.setSize(width, height);

            // Get the screen size
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Dimension screenSize = toolkit.getScreenSize();

            // Calculate the frame location
            int x = (screenSize.width - getWidth()) / 2;
            int y = (screenSize.height - getHeight()) / 2;

            // Set the new frame location
            setLocation(x, y);
        }

        @Override
        public void setSize(Dimension size) {
            setSize(size.width, size.height);
        }

        private void initGUI() {
            try {
                FlowLayout thisLayout = new FlowLayout();
                getContentPane().setLayout(thisLayout);
                {
                    infor = new JLabel();
                    getContentPane().add(infor);
                    infor.setText("Text Generation");
                    infor.setPreferredSize(new java.awt.Dimension(179, 35));
                    infor.setFont(new java.awt.Font("Arial", 1, 22));
                    infor.setForeground(new java.awt.Color(255, 0, 0));
                }
                {
                    lblRules = new JLabel();
                    getContentPane().add(lblRules);
                    lblRules.setLayout(null);
                    lblRules.setText("Version 1.0");
                    lblRules.setFont(new java.awt.Font("Arial", 1, 14));
                }
                {
                    jLabel2 = new JLabel();
                    getContentPane().add(jLabel2);
                    jLabel2.setText("\uu00a9 2008 Nguyen Vu Ngoc Tung, Nguyen Thien Binh. All rights reserved.");
                    jLabel2.setPreferredSize(new java.awt.Dimension(237, 32));
                }
                this.setSize(267, 139);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
